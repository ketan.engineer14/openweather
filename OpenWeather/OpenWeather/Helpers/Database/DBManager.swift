//
//  DBManager.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import UIKit
import CoreData
import Foundation

class DBManager {
    
    //MARK: - Properties
    static let shared = DBManager()
    private var appDelegate : AppDelegate
    private var managedContext : NSManagedObjectContext
    
    private init() {
        appDelegate =  UIApplication.shared.delegate as! AppDelegate
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    //MARK: - Functions
    func fetch<T: NSManagedObject>(entityName: T.Type) -> [T]? {
                
        let fetchRequest =
        NSFetchRequest<T>(entityName:  NSStringFromClass(T.self))
        
        do {
            let managedObjectArray = try managedContext.fetch(fetchRequest)
            return managedObjectArray
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    func save<T: NSManagedObject>(parameters: [String : Any], entityName: T.Type) {
        
        let entity =
        NSEntityDescription.entity(forEntityName: NSStringFromClass(T.self),
                                   in: managedContext)!
        
        let managedObject = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        for key in parameters.keys {
            managedObject.setValue(parameters[key], forKeyPath: key)
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func delete<T: NSManagedObject>(object: T, entityName: T.Type) {
        
        managedContext.delete(object)

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not delete. \(error), \(error.userInfo)")
        }
        
    }
    
    func containsRecord<T: NSManagedObject>(entityName: T.Type, longName : String) -> Bool {
        
        let fetchRequest =
        NSFetchRequest<T>(entityName:  NSStringFromClass(T.self))
        fetchRequest.predicate = NSPredicate(format: "longName == %@", longName)
        
        do {
            let managedObjectArray = try managedContext.fetch(fetchRequest)
            return managedObjectArray.count > 0 ? true : false
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return false
        }
    }
    
}

