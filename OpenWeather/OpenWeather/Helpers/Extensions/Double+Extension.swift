//
//  Double+Extension.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import Foundation

extension Double {
    
    func convertToCelsiusFromKelvin() -> String {
        return String(format: "%.0f", self - 273.15) + "°"
        
    }
    
}
