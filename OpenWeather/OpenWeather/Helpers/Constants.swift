//
//  Constants.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import UIKit

struct Constants {
    
    static let openWeatherAppId = "3fb13dc50738e3b81beec96fad953383"
    static let searchResultsLimit = "5"
    
    static let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
    
    static let addWeatherNotification = "com.ketan.OpenWeather.addWeatherNotification"
}
