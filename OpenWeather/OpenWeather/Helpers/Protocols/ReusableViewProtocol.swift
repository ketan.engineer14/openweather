//
//  ReusableViewProtocol.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 09/03/22.
//

import UIKit

protocol ReusableView : AnyObject {
    static var identifier : String { get }
    static var nib: UINib { get }
}
