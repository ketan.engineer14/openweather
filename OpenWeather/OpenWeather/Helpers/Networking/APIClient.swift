//
//  APIClient.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 05/03/22.
//

import Alamofire

class NetworkLogger: EventMonitor {

    let queue = DispatchQueue(label: "com.ketan.almofire.networklogger")

    func requestDidFinish(_ request: Request) {
        print(request.description)
    }

    func request<Value>(
        _ request: DataRequest,
        didParseResponse response: DataResponse<Value, AFError>
    ) {
        guard let data = response.data else {
            return
        }
        if let json = try? JSONSerialization
            .jsonObject(with: data, options: .mutableContainers) {
            print(json)
        }
    }
}

class APIClient {
    
    static let shared = APIClient()
    private init() {}
    
    var manager = Session()
    private func configureSession() {
        
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .returnCacheDataElseLoad
        if NetworkReachabilityManager()?.isReachable ?? false {
            configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        }
//        let handler = AccessTokenAdapter()
        manager = Session(configuration: configuration, interceptor: nil, eventMonitors: [NetworkLogger()])
        
        ///When we don't want network logs
//        let manager = Session(configuration: configuration, interceptor: nil)
        
    }
    
    // MARK: - Request
    func requestData(_ convertible: URLConvertible,
                     method: HTTPMethod = .get,
                     parameters: Parameters? = nil,
                     encoding: ParameterEncoding = URLEncoding.default,
                     headers: HTTPHeaders? = nil,
                     completion: @escaping (Result<Data, ErrorResult>) -> Void) {
        
        configureSession()
        manager.request(convertible,
                        method: method,
                        parameters: parameters,
                        encoding: encoding,
                        headers: headers).validate().responseData
        { (response) in
            switch response.result {
            case .success(let data):
                completion(.success(data))
            case .failure(let error):
                completion(.failure(.network(string: error.localizedDescription)))
            }
        }
        
    }
}
