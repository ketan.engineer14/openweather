//
//  APIConfiguration.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 05/03/22.
//

/// This will give base URL for API call according to the environment.
class APIConfiguration {
    
    /// Setup build environment for application.
    static let buildEnvironment: DevelopmentEnvironment = {
#if DEBUG
        print("DEBUG ENV ACTIVE")
        return .staging
#else
        print("PRODUCTION ENV ACTIVE")
        return .production
        
#endif
    }()
    
    static var baseURL: String {
        get {
            switch APIConfiguration.buildEnvironment {
            case .production:
                return "https://api.openweathermap.org/"
            case .staging:
                return "https://api.openweathermap.org/"
            }
        }
    }
    
    //
    private init() {
        
    }
}

enum DevelopmentEnvironment: String {
    ///
    case production = "Production"
    ///
    case staging = "Staging"
}


