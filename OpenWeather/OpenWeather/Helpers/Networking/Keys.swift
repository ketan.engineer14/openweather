//
//  Keys.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import Foundation

enum Keys {
    
    static let q = "q"
    static let limit = "limit"
    static let appId = "appid"
    static let lat = "lat"
    static let lon = "lon"
    static let name = "name"
    static let longName = "longName"
}
