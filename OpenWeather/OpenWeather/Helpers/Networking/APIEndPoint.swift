//
//  APIEndPoint.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 05/03/22.
//

enum APIEndPoint {
    
    case geo
    case weather
    
    var path: String {
        
        switch self {
        case .geo: return APIConfiguration.baseURL + "geo/1.0/direct"
        case .weather: return APIConfiguration.baseURL + "data/2.5/weather"
        }
    }
    
}
