//
//  RequestHandler.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 05/03/22.
//

import Foundation

class RequestHandler {
    func networkResultData<T : Decodable>(completion: @escaping ((Result<T, ErrorResult>) -> Void)) -> ((Result<Data, ErrorResult>) -> Void){
        return { dataResult in
            DispatchQueue.global(qos: .background).async(execute: {
                switch dataResult{
                case .success(let data):
                    //Converting the data into the Respective model
                    guard let userList = try? JSONDecoder().decode(T.self, from: data) else {
                        completion(.failure(.parser(string: "Unable to parse data")))
                        return
                    }
                    completion(.success(userList))
                case .failure(let error):
                    debugPrint("Network Error \(error)")
                    completion(.failure(.network(string: "Network Error " + error.localizedDescription)))
                    break
                }
            })
        }
    }
}

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}
