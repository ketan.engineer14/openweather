//
//  WeatherDetailsViewModel.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import Foundation

protocol WeatherDetailsViewDelegate: AnyObject {
    func refreshViewData()
}

class WeatherDetailsViewModel {
    
    //MARK: - Properties
    weak var viewDelegate: WeatherDetailsViewDelegate?
    private let service : WeatherDetailsApiService
    var weatherDetails: WeatherDetailsModel?
    var cityDetails : CityDetailsModelElement?
    var shouldHideAddButton: Bool
    
    var currentTemperature: String
    var weatherTitle: String
    var highLowTemperature: String
    var weatherDescription: String
    
    //MARK: - Init
    init() {
        self.service = WeatherDetailsApiService()
        currentTemperature = ""
        weatherTitle = ""
        highLowTemperature = ""
        weatherDescription = ""
        shouldHideAddButton = false
    }
    
    func start() {
        fetchWeatherDetailsWithLatLon()
    }
    
}

//MARK: - Functions
extension WeatherDetailsViewModel {
    
    private func fetchWeatherDetailsWithLatLon() {
        
        guard let lat = cityDetails?.lat, let lon = cityDetails?.lon else {
            return
        }
        
        service.fetchWeatherDetails(lat: String(lat), lon: String(lon)) { [weak self] result in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let successData):
                DispatchQueue.main.async {
                    strongSelf.weatherDetails = successData
                    strongSelf.setWeatherDetailsToDisplay()
                    strongSelf.viewDelegate?.refreshViewData()
                }
            case .failure(let error):
                print(error.localizedDescription)
                
            }
            
        }
        
    }
    
    private func setWeatherDetailsToDisplay() {
        
        if let temp = weatherDetails?.main?.temp {
            currentTemperature = temp.convertToCelsiusFromKelvin()
        }
        
        if let weathers = weatherDetails?.weather {
            if weathers.count > 0 {
                let weather = weathers[0]
                weatherTitle = weather.main ?? ""
                weatherDescription = weather.weatherDescription ?? ""
            }
        }
        
        if let high = weatherDetails?.main?.tempMax, let low = weatherDetails?.main?.tempMin {
            
            highLowTemperature = "H:\(high.convertToCelsiusFromKelvin())  L:\(low.convertToCelsiusFromKelvin())"
        }
        
    }
    
    func saveThisCity() {
        
        guard let lat = cityDetails?.lat, let lon = cityDetails?.lon, let name = cityDetails?.name, let longName = cityDetails?.getNameToDisplay() else {
            return
        }
        let parameters = [Keys.lat : lat, Keys.lon : lon, Keys.name : name, Keys.longName: longName] as [String : Any]
        DBManager.shared.save(parameters: parameters, entityName: Cities.self)
    }
    
}
