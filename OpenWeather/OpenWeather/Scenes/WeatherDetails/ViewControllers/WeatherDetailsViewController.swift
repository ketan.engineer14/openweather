//
//  WeatherDetailsViewController.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import UIKit

class WeatherDetailsViewController: UIViewController {
    
    //MARK: - Variables
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var currentTemperatureLabel: UILabel!
    @IBOutlet weak var weatherTitleLabel: UILabel!
    @IBOutlet weak var highLowTemperatureLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    lazy var viewModel: WeatherDetailsViewModel! = {
        var vm = WeatherDetailsViewModel()
        vm.viewDelegate = self
        return vm
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        viewModel.start()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Actions
    @IBAction func cancelButtonClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addButtonClick(_ sender: Any) {
        viewModel.saveThisCity()
        NotificationCenter.default.post(name: Notification.Name(Constants.addWeatherNotification), object: nibName)
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Setup UI
extension WeatherDetailsViewController {
    
    private func setupUI() {
        self.cityNameLabel.text = viewModel.cityDetails?.name
        self.currentTemperatureLabel.text = "--"
        self.weatherTitleLabel.text = ""
        self.highLowTemperatureLabel.text = ""
        self.weatherDescriptionLabel.text = ""
        
        if viewModel.shouldHideAddButton {
            addButton.isEnabled = false
            addButton.title = ""
        }
    }
}

// MARK: - WeatherDetailsViewDelegate
extension WeatherDetailsViewController : WeatherDetailsViewDelegate {
    
    func refreshViewData() {
        
        self.currentTemperatureLabel.text = viewModel.currentTemperature
        self.weatherTitleLabel.text = viewModel.weatherTitle
        self.highLowTemperatureLabel.text = viewModel.highLowTemperature
        self.weatherDescriptionLabel.text = viewModel.weatherDescription
    }
    
}
