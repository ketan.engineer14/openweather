//
//  WeatherDetailsApiService.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import Foundation

class WeatherDetailsApiService: RequestHandler {
    
    func fetchWeatherDetails(lat: String, lon: String, _ completion: @escaping ((Result<WeatherDetailsModel, ErrorResult>) -> Void)) {
        
        let parameters = [Keys.lat : lat, Keys.lon : lon, Keys.appId : Constants.openWeatherAppId]
        
        let endpoint = APIEndPoint.weather.path
        let networkResult = self.networkResultData(completion: completion)
        APIClient.shared.requestData(endpoint, parameters: parameters, completion: networkResult)
    }
    
}
