//
//  CityDetailsModel.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import Foundation

// MARK: - CityDetailsModelElement
struct CityDetailsModelElement: Codable {
    let name: String?
    let lat, lon: Double?
    let country, state: String?
    
    func getNameToDisplay() -> String {
        var name = self.name ?? ""
        if let state = self.state {
            name += "," + " " + state
        }
        if let country = self.country {
            name += "," + " " + country
        }
        
        return name
    }
}

typealias CityDetailsModel = [CityDetailsModelElement]
