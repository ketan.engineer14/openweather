//
//  SearchResultsViewModel.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import Foundation

protocol SearchResultsViewDelegate: AnyObject {
    func refreshViewData()
}

class SearchResultsViewModel {
    
    //MARK: - Properties
    weak var viewDelegate: SearchResultsViewDelegate?
    var cityDetails: CityDetailsModel?
    let service : SearchResultsApiService
    var currentQueryToSerch = ""
    
    //MARK: - Init
    init() {
        self.service = SearchResultsApiService()
    }
    
}

//MARK: - Functions
extension SearchResultsViewModel {
    
    func fetchCitiesWithSearchText(query : String) {
        
        currentQueryToSerch = query
        
        if query == "" {
            cityDetails = []
            viewDelegate?.refreshViewData()
            return
        }
        
        service.fetchCityDetails(query: query) { [weak self] result in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let successData):
                DispatchQueue.main.async {
                    strongSelf.cityDetails = successData
                    strongSelf.viewDelegate?.refreshViewData()
                }
            case .failure(let error):
                print(error.localizedDescription)
                
            }
            
        }
        
    }
    
    func getNameToDisplay(index: Int) -> String {
        
        let cityDetails = cityDetails?[index]
        return cityDetails?.getNameToDisplay() ?? ""
    }
}
