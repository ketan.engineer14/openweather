//
//  SearchResultsRouter.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import UIKit

enum SearchResultsRoute: String {
    case weatherDetail
}

protocol SearchResultsRouter {
    func route(
        to routeID: SearchResultsRoute,
        from source: SearchResultViewController,
        parameters: CityDetailsModelElement?
    )
}

class DefaultSearchResultsRouter : SearchResultsRouter {
    
    func route(to routeID: SearchResultsRoute, from source: SearchResultViewController, parameters: CityDetailsModelElement?) {
        
        switch routeID {
            
        case .weatherDetail:
            if let weatherDetailsVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WeatherDetailsViewController") as? WeatherDetailsViewController , let parameters = parameters {
                weatherDetailsVC.viewModel.cityDetails = parameters
                let shouldHide = DBManager.shared.containsRecord(entityName: Cities.self, longName: parameters.getNameToDisplay())
                weatherDetailsVC.viewModel.shouldHideAddButton = shouldHide
                source.present(weatherDetailsVC, animated: true, completion: nil)
            }
        }
    }
}
