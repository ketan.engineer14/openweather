//
//  SearchResultViewController.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import UIKit

class SearchResultViewController: UIViewController {
    
    //MARK: - Variables
    @IBOutlet weak var cityListTableView: UITableView!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    lazy var viewModel: SearchResultsViewModel! = {
        var vm = SearchResultsViewModel()
        vm.viewDelegate = self
        return vm
    }()
    
    lazy var router: SearchResultsRouter! = {
        let router = DefaultSearchResultsRouter()
        return router
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(dismissViewController), name: NSNotification.Name(Constants.addWeatherNotification), object: nil)
        setupUI()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(Constants.addWeatherNotification), object: nil)
    }
    
    @objc func dismissViewController(_ notification: Notification) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Setup UI
extension SearchResultViewController {
    
    private func setupUI() {
        
        noResultsLabel.isHidden = true
        cityListTableView.dataSource = self
        cityListTableView.delegate = self
    }
}

// MARK: - Tableview Datasource and Delegate
extension SearchResultViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cityDetails?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CityNameTableViewCell.identifier, for: indexPath) as? CityNameTableViewCell else {
            return UITableViewCell()
        }
        cell.configureCell(name: viewModel.getNameToDisplay(index: indexPath.row))
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let city = viewModel.cityDetails?[indexPath.row] else {
            return
            
        }
        router.route(to: .weatherDetail, from: self, parameters: city)
    }
    
}


// MARK: - UISearchResultsUpdating
extension SearchResultViewController : UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        noResultsLabel.isHidden = true
        viewModel.fetchCitiesWithSearchText(query: searchController.searchBar.text ?? "")
        
    }
    
}

// MARK: - SearchResultsViewDelegate
extension SearchResultViewController : SearchResultsViewDelegate {
    
    func refreshViewData() {
        noResultsLabel.text = "No results found for \"\(viewModel.currentQueryToSerch)\"."
        guard let cityDetails = viewModel.cityDetails else {
            noResultsLabel.isHidden = true
            return
        }
        noResultsLabel.isHidden = cityDetails.count == 0 ? false : true
        cityListTableView.reloadData()
    }
    
}


