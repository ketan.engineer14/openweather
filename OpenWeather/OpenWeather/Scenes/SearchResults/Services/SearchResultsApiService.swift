//
//  SearchResultsApiService.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

class SearchResultsApiService: RequestHandler {
    
    func fetchCityDetails(query: String, _ completion: @escaping ((Result<CityDetailsModel, ErrorResult>) -> Void)) {
        
        let parameters = [Keys.q : query, Keys.limit : Constants.searchResultsLimit, Keys.appId : Constants.openWeatherAppId]

        let endpoint = APIEndPoint.geo.path
        let networkResult = self.networkResultData(completion: completion)
        APIClient.shared.requestData(endpoint, parameters: parameters, completion: networkResult)
        
    }
    
}
