//
//  WeatherListRouter.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 09/03/22.
//

import UIKit

enum WeatherListRoute: String {
    case weatherDetail
}

protocol WeatherListRouter {
    func route(
        to routeID: WeatherListRoute,
        from source: WeatherListViewController,
        parameters: CityDetailsModelElement?
    )
}

class DefaultWeatherListRouter : WeatherListRouter {
    
    func route(to routeID: WeatherListRoute, from source: WeatherListViewController, parameters: CityDetailsModelElement?) {
        
        switch routeID {
            
        case .weatherDetail:
            if let weatherDetailsVC = Constants.mainStoryboard.instantiateViewController(withIdentifier: "WeatherDetailsViewController") as? WeatherDetailsViewController , let parameters = parameters {
                weatherDetailsVC.viewModel.cityDetails = parameters
                weatherDetailsVC.viewModel.shouldHideAddButton = true
                source.present(weatherDetailsVC, animated: true, completion: nil)
            }
        }
    }
}

