//
//  WeatherTableViewCell.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 07/03/22.
//

import UIKit

class WeatherListTableViewCell: UITableViewCell, ReusableView {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    /// Return Identifier
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(name: String) {
        self.cityNameLabel.text = name
    }
    
}
