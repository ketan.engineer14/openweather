//
//  ViewController.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 05/03/22.
//

import UIKit

class WeatherListViewController: UIViewController {
    
    //MARK: - Variables
    private var searchController = UISearchController()
    @IBOutlet weak var weatherListTableView: UITableView!
    
    
    lazy var viewModel: WeatherListViewModel! = {
        var vm = WeatherListViewModel()
        vm.viewDelegate = self
        return vm
    }()
    
    lazy var router: WeatherListRouter! = {
        let router = DefaultWeatherListRouter()
        return router
    }()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(clearSearchBarText), name: NSNotification.Name(Constants.addWeatherNotification), object: nil)
        setupUI()
        viewModel.start()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(Constants.addWeatherNotification), object: nil)
    }
    
    @objc func clearSearchBarText() {
        searchController.searchBar.text = ""
        viewModel.start()
        
    }
}

//MARK: - Setup UI
extension WeatherListViewController {
    
    private func setupUI() {
        
        guard let searchResultViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultViewController") as? SearchResultViewController else {
            return
        }
        
        self.searchController = ({
            let controller = UISearchController(searchResultsController: searchResultViewController)
            controller.searchResultsUpdater = searchResultViewController
            controller.obscuresBackgroundDuringPresentation = true;
            self.navigationItem.searchController = controller
            return controller
        })()
        
        weatherListTableView.dataSource = self
        weatherListTableView.delegate = self
    }
    
}

// MARK: - Tableview Datasource and Delegate
extension WeatherListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cities?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: WeatherListTableViewCell.identifier, for: indexPath) as? WeatherListTableViewCell, let city = viewModel.cities?[indexPath.row] else {
            return UITableViewCell()
        }
        cell.configureCell(name: city.longName ?? "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cityDetails = viewModel.getCityDetailsObjectFor(index: indexPath.row) else {
            return
        }
        router.route(to: .weatherDetail, from: self, parameters: cityDetails)
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            
            tableView.performBatchUpdates {
                viewModel.deleteObjectFor(index: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            } completion: { success in
                
            }
            
        }
    }
    
}

// MARK: - WeatherListViewDelegate
extension WeatherListViewController: WeatherListViewDelegate {
    
    func reloadTableData() {
        weatherListTableView.reloadData()
    }
    
}
