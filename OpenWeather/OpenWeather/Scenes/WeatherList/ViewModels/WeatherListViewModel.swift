//
//  WeatherListViewModel.swift
//  OpenWeather
//
//  Created by Ketan Parmar on 08/03/22.
//

import Foundation

protocol WeatherListViewDelegate: AnyObject {
    func reloadTableData()
}

class WeatherListViewModel {
    
    //MARK: - Properties
    weak var viewDelegate: WeatherListViewDelegate?
    var cities : [Cities]?
    
    //MARK: - Init
    init() {
    }
    
    func start() {
        fetchStoredWeatherList()
    }
    
}

//MARK: - Functions
extension WeatherListViewModel {
    
    private func fetchStoredWeatherList() {
        
        if let citiest = DBManager.shared.fetch(entityName: Cities.self) {
            self.cities = citiest
            DispatchQueue.main.async {
                self.viewDelegate?.reloadTableData()
            }
            
        }
    }
    
    func getCityDetailsObjectFor(index: Int) -> CityDetailsModelElement? {
        
        guard let city = cities?[index] else {
            return nil
        }
        
        let cityDetails = CityDetailsModelElement(name: city.name, lat: city.lat, lon: city.lon, country: nil, state: nil)
        return cityDetails
        
    }
    
    func deleteObjectFor(index: Int) {
        
        guard let city = cities?[index] else {
            return
            
        }
        cities?.remove(at: index)
        DBManager.shared.delete(object: city, entityName: Cities.self)
    }
    
}
